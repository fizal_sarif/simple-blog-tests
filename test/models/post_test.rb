require 'test_helper'

class PostTest < ActiveSupport::TestCase

  # setup method runs before every test method below
  def setup
    @user = User.first
    @post = @user.posts.create(content: "hello world")
  end

  # teardown method runs after every test method below.
  def teardown
    Post.delete_all
  end

  # words.yml contains valid words for testing

  ## basic tests

  test "post has content column" do
    assert @post.respond_to?(:content)
  end

  test "post belongs_to user" do
    assert_equal @user, @post.user
  end

  test "saves valid post with content" do
    assert @post.save
    assert_equal @post.content, "hello world"
  end

  test "don't save post without content or user" do
    @post.content = ""
    assert_not @post.save, "Saved post without content"
    post1 = Post.new(content: "hello")
    assert_not post1.save, "Saved post without user."
  end

  ## tests for simple words only

  test "don't save post with invalid words" do
    post = @user.posts.new(content: "zxc")
    assert_not post.save, "saved post with invalid word"
    post.content = "excellent"
    assert_not post.save
  end

  test "save posts intact with capital punctuation" do
    hamlet = "To BE or NOT to BE"
    @post.content = hamlet
    assert @post.save
    assert_equal hamlet, @post.content

    punc = "they're hello?!"
    @post.content = punc
    assert @post.save
    assert_equal punc, @post.content
  end

  test "keep newlines" do
    poem = "hello\nhello word\nhello"
    @post.content = poem
    @post.save
    assert_equal poem, @post.content
  end

  test "add invalid words to errors" do
    @post.content = "hello excellent yellow world to be lemon"
    @post.save
    error_message = @post.errors.messages[:content].first
    assert_equal("Not found: 'excellent' 'yellow' 'lemon'", error_message) #
  end

  test "suggest splitting words when possible" do
    @post.content = "helloworld tobeornot"
    @post.save
    error_message = @post.errors.messages[:content].first
    assert_equal("Not found: 'helloworld' (try splitting into: hello world) 'tobeornot' (try splitting into: to be or not)", error_message) #

    @post.content = "tobetobebebe"
    @post.save
    error_message = @post.errors.messages[:content].first
    assert_equal("Not found: 'tobetobebebe' (try splitting into: to be to be be be)", error_message)
  end

end